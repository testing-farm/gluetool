``gluetool`` features
=====================

A comprehensive list of ``gluetool`` features, available helpers, tricks and tips. All the ways ``gluetool`` have to help module developers.

.. include:: features/core.rst
.. include:: features/help.rst
.. include:: features/logging.rst
.. include:: features/colors.rst
.. include:: features/sentry.rst
.. include:: features/utils.rst


----

Tool
----

.. todo::

  Features yet to describe:

  * reusable heart of gluetool
  * config file on system level, user level or in a local dir


----

.. todo::

  Features yet to describe:

  * custom pylint checkers
  * option names
  * shared function definitions
